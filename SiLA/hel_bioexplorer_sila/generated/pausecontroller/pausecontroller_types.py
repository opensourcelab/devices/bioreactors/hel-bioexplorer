# Generated by sila2.code_generator; sila2.__version__: 0.9.2
from __future__ import annotations

from typing import NamedTuple


class Pause_Responses(NamedTuple):

    pass


class Resume_Responses(NamedTuple):

    pass


UUID = str
