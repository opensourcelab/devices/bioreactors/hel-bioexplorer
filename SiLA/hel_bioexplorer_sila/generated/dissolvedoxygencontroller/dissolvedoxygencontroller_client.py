# Generated by sila2.code_generator; sila2.__version__: 0.9.2
# -----
# This class does not do anything useful at runtime. Its only purpose is to provide type annotations.
# Since sphinx does not support .pyi files (yet?), so this is a .py file.
# -----

from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:

    from sila2.client import ClientObservableProperty


class DissolvedOxygenControllerClient:
    """

    Controlling the dissolved oxygen in a reactor.

    """

    DissolvedOxygen: ClientObservableProperty[float]
    """
    Current Dissolved Oxygen
    """
